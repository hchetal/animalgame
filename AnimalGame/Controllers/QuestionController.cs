﻿using AnimalGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AnimalGame.Controllers
{
    public class QuestionController : Controller
    {
        // GET: Question

        private AnimalGameDataEntities db = new AnimalGameDataEntities();

        public ActionResult Index()
        {
            var distinctColors = (from col in db.Animals
                                 select col.Color).Distinct();
            
            var distinctFeatures= (from col in db.Animals
                                  select col.Feature).Distinct();

            var distinctSounds = (from col in db.Animals
                                   select col.Sound).Distinct();

            ViewBag.Colors = distinctColors;
            ViewBag.Features = distinctFeatures;
            ViewBag.Sounds = distinctSounds;

            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            try
            {
                var selectedColor = form["AnimalColor"].ToString();

                var selectedFeature = form["AnimalFeature"].ToString();

                var selectedSound = form["AnimalSound"].ToString();

                string chosenAnimal = (from anim in db.Animals.Where(x => x.Color == selectedColor && x.Sound == selectedSound && x.Feature == selectedFeature)
                                       select anim.Name).FirstOrDefault();



                if (string.IsNullOrEmpty(chosenAnimal))
                    ViewBag.Message = "No matching animal found";
                else
                    ViewBag.Message = "You thought of animal " + chosenAnimal.ToString();
            }
            catch (NullReferenceException ex)
            {
                ViewBag.Message = "No matching animal found";

            }
            catch(Exception ex)
            {
                ViewBag.Message = "Error in the application. please try again.";
            }
             return View("Result");
            
        }
    }
}