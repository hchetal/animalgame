﻿CREATE TABLE [dbo].[Animal] (
    [Id]      INT          NOT NULL IDENTITY,
    [Name]    VARCHAR (50) NOT NULL,
    [Color]   VARCHAR (50) NOT NULL,
    [Feature] VARCHAR (50) NOT NULL,
    [Sound]   VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

